package service

import (
	"bytes"
	"consumer/pkg/rabbitmq"
	"context"
	"encoding/json"
	"net/http"
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/goxp/cloud0/logger"
)

type EventHandler struct {
	cfg    *EventRoute
	client *http.Client
}

func NewHandler(cfg *EventRoute) *EventHandler {
	return &EventHandler{
		cfg:    cfg,
		client: &http.Client{},
	}
}

func (h *EventHandler) Handle(d rabbitmq.Delivery) bool {
	log := logger.Tag("EventHandler.Handle")

	var (
		msgID string
		start = time.Now()
	)
	if v_, ok := d.Headers["x-request-id"]; !ok || v_.(string) == "" {
		id_, err := uuid.NewV1()
		if err != nil {
			log.WithError(err).Error("failed to set x-request-id")
		}
		msgID = id_.String()
	}

	log = log.WithField("x-request-id", msgID)
	log.
		WithField("route", d.RoutingKey).
		WithField("url", h.cfg.URL).
		WithField("body", string(d.Body)).Info("sending")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.TimeoutSeconds))
	defer cancel()

	payload := EmitRequest{
		Topic: d.RoutingKey,
		Body:  d.Body,
	}

	raw, _ := json.Marshal(payload)

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, h.cfg.URL, bytes.NewReader(raw))
	if err != nil {
		log.WithError(err).Error("failed to create request")
		return true
	}

	req.Header.Set("user-agent", "consumer/v1")
	req.Header.Set("x-request-id", msgID)
	req.Header.Set("content-type", "application/json")

	rsp, err := h.client.Do(req)
	log = log.WithField("latency", time.Since(start).String())
	if err != nil {
		log.WithError(err).Error("failed to send request")
		return true
	}

	log.WithField("resp.status", rsp.StatusCode).Info("success send request")

	return true
}
