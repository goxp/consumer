package service

import (
	"gopkg.in/yaml.v3"
)

type RoutesConfig []*EventRoute

type EventRoute struct {
	Routes         []string `yaml:"routes"`
	URL            string   `yaml:"url"`
	Concurrency    int      `yaml:"concurrency"`
	Exchange       string   `yaml:"exchange"`
	Queue          string   `yaml:"queue"`
	TimeoutSeconds int      `yaml:"timeout_seconds"`
}

func UnmarshalRoutes(data []byte) (RoutesConfig, error) {
	var routes RoutesConfig
	err := yaml.Unmarshal(data, &routes)
	return routes, err
}
