package service

import (
	"consumer/pkg/rabbitmq"
	"encoding/json"

	"gitlab.com/goxp/cloud0/ginext"
)

type EmitRequest struct {
	Topic string          `json:"topic" validate:"required,min=3"`
	Body  json.RawMessage `json:"body"`
}

type httpHandler struct {
	pub *rabbitmq.Publisher
}

func NewHttpHandler(pub *rabbitmq.Publisher) *httpHandler {
	return &httpHandler{
		pub: pub,
	}
}

func (h *httpHandler) Emit(r *ginext.Request) (*ginext.Response, error) {
	req := EmitRequest{}
	r.MustBind(&req)

	err := h.pub.Publish(
		req.Body,
		[]string{req.Topic},
		rabbitmq.WithPublishOptionsContentType("application/json"),
		rabbitmq.WithPublishOptionsMandatory,
		rabbitmq.WithPublishOptionsPersistentDelivery,
		rabbitmq.WithPublishOptionsExchange("events"),
	)
	r.MustNoError(err)
	return ginext.NewResponse(200), nil
}
