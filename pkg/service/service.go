package service

import (
	"consumer/pkg/rabbitmq"
	"context"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"github.com/caarlos0/env/v6"
	"github.com/gin-gonic/gin"
	"github.com/streadway/amqp"
	"gitlab.com/goxp/cloud0/ginext"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/goxp/cloud0/service"
)

var (
	name    = "consumer"
	version = "v1.0.0"
)

type appSettings struct {
	QueueURL         string `env:"QUEUE_URL"`
	ConfigRoutesPath string `env:"CONFIG_ROUTES_PATH" envDefault:"config/config.yml"`
}

type Service struct {
	*service.BaseApp
	*appSettings

	publisher    *rabbitmq.Publisher
	consumer     *rabbitmq.Consumer
	routesConfig RoutesConfig
}

func newService() *Service {
	s := &Service{
		BaseApp:     service.NewApp(name, version),
		appSettings: &appSettings{},
	}

	if err := s.Initialize(); err != nil {
		panic("failed to initialize service: " + err.Error())
	}

	if err := env.Parse(s.appSettings); err != nil {
		panic("failed to parse service extra setting: " + err.Error())
	}

	conf := amqp.Config{
		Heartbeat: time.Second * 30,
	}
	publisher, _, err := rabbitmq.NewPublisher(
		s.QueueURL,
		conf,
		rabbitmq.WithPublisherOptionsLogger(logger.Tag("pub")),
	)
	if err != nil {
		panic("failed to init pub: " + err.Error())
	}
	consumer, err := rabbitmq.NewConsumer(
		s.QueueURL,
		conf,
		rabbitmq.WithConsumerOptionsLogger(logger.Tag("consumer")),
	)
	if err != nil {
		panic("failed to init consumer: " + err.Error())
	}

	s.publisher = publisher
	s.consumer = consumer

	if err := s.setupRoutesConfig(); err != nil {
		panic(err)
	}

	httpHandler := NewHttpHandler(s.publisher)
	s.Router.POST("/events", ginext.WrapHandler(httpHandler.Emit))

	return s
}

func (s *Service) setupRoutesConfig() error {
	log := logger.Tag("Service.setupRoutesConfig")
	log.Infof("log config routes from: %s", s.ConfigRoutesPath)
	data, err := ioutil.ReadFile(s.ConfigRoutesPath)
	if err != nil {
		return fmt.Errorf("failed to read config file [%s]: %v", s.ConfigRoutesPath, err)
	}
	configRoutes, err := UnmarshalRoutes(data)
	if err != nil {
		log.WithField("configData", string(data)).Debug("failed to decode config")
		return fmt.Errorf("failed to decode config data: %v", err)
	}
	s.routesConfig = configRoutes
	err = s.registerConsumers()
	if err != nil {
		return err
	}

	return nil
}

func (s *Service) registerConsumers() error {
	log := logger.Tag("Service.registerConsumers")

	log.WithField("routes", len(s.routesConfig)).Info("registering event routes")
	for i, route := range s.routesConfig {
		if route.Exchange == "" {
			route.Exchange = "events"
		}
		if route.Queue == "" {
			route.Queue = fmt.Sprintf("%s:%d", route.Exchange, i)
		}
		if route.TimeoutSeconds == 0 {
			route.TimeoutSeconds = 30
		}

		handler := NewHandler(route)
		log.WithField("queue", route.Queue).WithField("routes", strings.Join(route.Routes, ", ")).Info("consuming")

		err := s.consumer.StartConsuming(
			handler.Handle,
			route.Queue,
			route.Routes,
			rabbitmq.WithConsumeOptionsConcurrency(route.Concurrency),
			rabbitmq.WithConsumeOptionsBindingExchangeSkipDeclare,
			rabbitmq.WithConsumeOptionsBindingExchangeName(route.Exchange),
			rabbitmq.WithConsumeOptionsQueueDurable,
			rabbitmq.WithConsumeOptionsConsumerName(route.Queue),
		)

		if err != nil {
			return fmt.Errorf("failed to consume routes: %s, err: %v", strings.Join(route.Routes, ", "), err)
		}
	}

	return nil
}

// Run initializes a new service and start serving
func Run() error {
	gin.SetMode(gin.ReleaseMode)
	logger.Init(name)
	return newService().Start(context.Background())
}
