package rabbitmq

import (
	"errors"
	"math/rand"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

type channelManager struct {
	logger              *logrus.Entry
	url                 string
	channel             *amqp.Channel
	connection          *amqp.Connection
	config              amqp.Config
	channelMux          *sync.RWMutex
	notifyCancelOrClose chan error
}

func newChannelManager(url string, config amqp.Config, logger *logrus.Entry) (*channelManager, error) {
	conn, ch, err := getNewChannel(url, config)
	if err != nil {
		return nil, err
	}

	m := channelManager{
		logger:              logger,
		url:                 url,
		connection:          conn,
		channel:             ch,
		channelMux:          &sync.RWMutex{},
		config:              config,
		notifyCancelOrClose: make(chan error),
	}

	go m.startNotifyCancelOrClose()

	return &m, nil
}

func getNewChannel(url string, conf amqp.Config) (*amqp.Connection, *amqp.Channel, error) {
	conn, err := amqp.DialConfig(url, conf)
	if err != nil {
		return nil, nil, err
	}

	ch, err := conn.Channel()
	if err != nil {
		return nil, nil, err
	}

	return conn, ch, err
}

// startNotifyCancelOrClose listens on the channel's cancelled and closed notifiers.
// When it detects a problem, it attempts to reconnect with an exponential backoff.
// Once reconnected, it sends an error back on the manager's notifyCancelOrClose channel
func (m *channelManager) startNotifyCancelOrClose() {
	notifyCloseChan := m.channel.NotifyClose(make(chan *amqp.Error, 1))
	notifyCancelChan := m.channel.NotifyCancel(make(chan string, 1))

	select {
	case err := <-notifyCloseChan:
		// If the connection close is triggered by the server, a reconnection takes place
		if err != nil && err.Server {
			m.logger.Info("attempting to reconnect to amqp server after close")
			m.reconnectWithBackoff()
			m.logger.Info("successfully reconnected to amqp server after close")
			m.notifyCancelOrClose <- err
		}
	case err := <-notifyCancelChan:
		m.logger.Info("attempting to reconnect to amqp server after cancel")
		m.reconnectWithBackoff()
		m.logger.Info("successfully reconnected to amqp server after cancel")
		m.notifyCancelOrClose <- errors.New(err)
	}
}

// reconnectWithBackoff continuously attempts to reconnect with an exponential backoff strategy
func (m *channelManager) reconnectWithBackoff() {
	backoffTime := time.Second
	rand.Seed(time.Now().Unix())

	for {
		m.logger.Infof("waiting %s to attempt to reconnect to amqp server", backoffTime.String())

		// limit max waiting at 120s
		if backoffTime < 120*time.Second {
			time.Sleep(backoffTime + time.Millisecond*time.Duration(rand.Intn(99)+1))
			backoffTime *= 2
		}
		err := m.reconnect()
		if err != nil {
			m.logger.Infof("error reconnecting to amqp server: %v", err)
			continue
		}

		// success
		return
	}
}

// reconnect safely closes the current channel and obtains a new one
func (m *channelManager) reconnect() error {
	m.channelMux.Lock()
	defer m.channelMux.Unlock()
	newConn, newChannel, err := getNewChannel(m.url, m.config)
	if err != nil {
		return err
	}

	_ = m.channel.Close()
	_ = m.connection.Close()
	m.connection = newConn
	m.channel = newChannel
	go m.startNotifyCancelOrClose()
	return nil
}

// Close stops connection
func (m *channelManager) Close() {
	_ = m.channel.Close()
	_ = m.connection.Close()
}
