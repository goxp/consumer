package rabbitmq

import (
	"context"
	"errors"
	"io"
	"math/rand"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// Consumer allows you to create and connect to queues for data consumption.
type Consumer struct {
	chManager *channelManager
	logger    *logrus.Entry
}

// ConsumerOptions are used to describe a consumer's configuration.
// Logging set to true will enable consumer to print to stdout
type ConsumerOptions struct {
	Logging bool
	Logger  *logrus.Entry
}

// Delivery captures the fields for a previously delivered message resident in a queue
// to be delivered by the server to a consumer from Channel.Consume or Channel.Get
type Delivery struct {
	amqp.Delivery
}

func NewConsumer(url string, config amqp.Config, optionFuncs ...func(options *ConsumerOptions)) (*Consumer, error) {
	options := &ConsumerOptions{}
	for _, fn := range optionFuncs {
		fn(options)
	}
	if options.Logger == nil {
		log := logrus.New()
		log.SetOutput(io.Discard)
		options.Logger = log.WithContext(context.Background())
	}

	chManager, err := newChannelManager(url, config, options.Logger)
	if err != nil {
		return nil, err
	}
	consumer := &Consumer{
		chManager: chManager,
		logger:    options.Logger,
	}

	return consumer, nil
}

// WithConsumerOptionsLogging enables logging
func WithConsumerOptionsLogging(options *ConsumerOptions) {
	options.Logging = true
	options.Logger = logrus.WithContext(context.Background())
}

// WithConsumerOptionsLogger set custom logger
func WithConsumerOptionsLogger(logger *logrus.Entry) func(options *ConsumerOptions) {
	return func(options *ConsumerOptions) {
		if logger != nil {
			options.Logging = true
			options.Logger = logger
		}
	}
}

// MessageHandler describes an incoming message handler
type MessageHandler func(d Delivery) bool

// StartConsuming starts n goroutines where n=ConsumerOptions.QosOptions.Concurrency
// Each goroutine spawns a handler that consumes off of the given queue which binds to
// the routing key(s). The provided handler is called once for each message.
// If the provided queue doesn't exist, it will be created on the cluster
func (c *Consumer) StartConsuming(
	handler MessageHandler,
	queue string,
	routingKeys []string,
	optionFuncs ...func(*ConsumeOptions),
) error {
	defaultOptions := getDefaultConsumeOptions()
	options := &ConsumeOptions{}
	for _, fn := range optionFuncs {
		fn(options)
	}
	if options.Concurrency < 1 {
		options.Concurrency = defaultOptions.Concurrency
	}

	if queue == "" && !options.QueueAutoDelete {
		c.logger.Warn("start consuming with an empty queue name & no auto_delete, this might cause many generated queues")
	}

	if options.ConsumerName == "" {
		c.logger.Warn("start consuming with an empty consumer name or tag, you'll not able to stop this individually")
	}

	err := c.startGoroutines(handler, queue, routingKeys, *options)
	if err != nil {
		return err
	}

	go func() {
		for err := range c.chManager.notifyCancelOrClose {
			c.logger.Errorf("consume cancel/close handler trigger. err: %v", err)
			c.startGoroutinesWithRetries(handler, queue, routingKeys, *options)
		}
	}()
	return nil
}

// Disconnect disconnects both the channel and the connection
// this method doesn't throw a reconnect, and should be used when finishing a program.
// IMPORTANT: if this method is executed before StopConsuming, it could cause unexpected
// behavior such as messages being processed, but not being acknowledged, thus being requested
// by the broker
func (c *Consumer) Disconnect() {
	c.chManager.Close()
}

// StopConsuming stops the consumption of messages.
// the consumer should be discarded as it's not safe for re-use
// this method sends a basic.cancel notification.
// the consumerName is the name or delivery tag of the amqp consumer we want to cancel.
// when noWait is true, do not wait for the server to acknowledge the cancel.
// Only use this when you are certain there are no deliveries and be dropped in the
// client without an ack, and will not be redelivered to other consumers.
// IMPORTANT: since the streadway library doesn't provide a way to retrieve the consumer's tag
// after the creation. It's imperative for you to set the name when creating the consumer,
// if you want to use this function later a simple uuid4 should do the trick, since it should be unique.
// If you start many consumers, you should store the name of the consumers when creating them,
// such that you can use them in a for to stop all the consumers.
func (c *Consumer) StopConsuming(consumerName string, noWait bool) {
	_ = c.chManager.channel.Cancel(consumerName, noWait)
}

// startGoroutinesWithRetries attempts to start consuming on a channel
// with an exponential backoff
func (c *Consumer) startGoroutinesWithRetries(
	handler MessageHandler,
	queue string,
	routingKeys []string,
	options ConsumeOptions,
) {
	rand.Seed(time.Now().Unix())
	backoffTime := time.Second
	for {
		c.logger.Infof("waiting %s to attempt to start consumer goroutines", backoffTime.String())
		time.Sleep(backoffTime + time.Millisecond*time.Duration(rand.Intn(99)+1))
		backoffTime *= 2
		err := c.startGoroutines(handler, queue, routingKeys, options)
		if err != nil {
			c.logger.Errorf("couldn't start consumer goroutines. err: %v", err)
			continue
		}
		// success
		break
	}
}

// startGoroutines declares the queue if it doesn't exist,
// binds the queue to the routing keys, and starts the goroutines
// that will consume from the queue
func (c *Consumer) startGoroutines(
	handler MessageHandler,
	queue string,
	routingKeys []string,
	consumeOptions ConsumeOptions,
) error {
	c.chManager.channelMux.RLock()
	defer c.chManager.channelMux.RUnlock()

	_, err := c.chManager.channel.QueueDeclare(
		queue,
		consumeOptions.QueueDurable,
		consumeOptions.QueueAutoDelete,
		consumeOptions.QueueExclusive,
		consumeOptions.QueueNoWait,
		tableToAMQPTable(consumeOptions.QueueArgs),
	)
	if err != nil {
		return err
	}

	if consumeOptions.BindingExchange != nil {
		exchange := consumeOptions.BindingExchange
		if exchange.Name == "" {
			return errors.New("binding to exchange but name not specified")
		}

		if exchange.Declare {
			err = c.chManager.channel.ExchangeDeclare(
				exchange.Name,
				exchange.Kind,
				exchange.Durable,
				exchange.AutoDelete,
				exchange.Internal,
				exchange.NoWait,
				tableToAMQPTable(exchange.ExchangeArgs),
			)
			if err != nil {
				return err
			}
		}
		for _, routingKey := range routingKeys {
			err = c.chManager.channel.QueueBind(
				queue,
				routingKey,
				exchange.Name,
				consumeOptions.BindingNoWait,
				tableToAMQPTable(consumeOptions.BindingArgs),
			)
			if err != nil {
				return err
			}
		}
	}
	err = c.chManager.channel.Qos(
		consumeOptions.QosPrefetch,
		0,
		consumeOptions.QosGlobal,
	)
	if err != nil {
		return err
	}
	msgs, err := c.chManager.channel.Consume(
		queue,
		consumeOptions.ConsumerName,
		consumeOptions.ConsumerAutoAck,
		consumeOptions.ConsumerExclusive,
		consumeOptions.ConsumerNoLocal,
		consumeOptions.ConsumerNoWait,
		tableToAMQPTable(consumeOptions.ConsumerArgs),
	)
	if err != nil {
		return err
	}

	for i := 0; i < consumeOptions.Concurrency; i++ {
		go func() {
			for msg := range msgs {
				if consumeOptions.ConsumerAutoAck {
					handler(Delivery{msg})
					continue
				}
				if handler(Delivery{msg}) {
					err := msg.Ack(false)
					if err != nil {
						c.logger.Errorf("can't ack message: %v", err)
					}
				} else {
					err := msg.Nack(false, true)
					if err != nil {
						c.logger.Errorf("can't nack message: %v", err)
					}
				}
			}
			c.logger.Info("rabbit consumer goroutine closed")
		}()
	}
	c.logger.
		WithField("keys", strings.Join(routingKeys, ", ")).
		WithField("exchange", consumeOptions.BindingExchange.Name).
		Infof("processing messages on %v goroutines", consumeOptions.Concurrency)

	return nil
}
