package rabbitmq

import (
	"context"
	"errors"
	"sync"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// DeliveryMode.
// Transient means higher throughput but messages will not be restored on broker restart.
// The delivery mode of publishing is unrelated to the durability of the queues the reside on.
// Transient messages will not be restored to durable queues, persistent messages will be
// restored to durable queues and lost on non-durable queues during server restart.
//
// This remains typed as uint8 to match Publishing.DeliveryMode. Other delivery modes
// specific to custom queue implementations are not enumerated here.
const (
	Transient  uint8 = amqp.Transient
	Persistent uint8 = amqp.Persistent
)

// Return captures a flattened struct of fields returned by the server when a
// Publishing is unable to be delivered either due to the `mandatory` flag set
// and no route found, or `immediate` flag set and no free consumer.
type Return struct {
	amqp.Return
}

type PublishOptions struct {
	Exchange string
	// Mandatory fails to publish if there are no queues
	// bound to the routing key
	Mandatory bool
	// Immediate fails to publish if there are no consumers
	// that can ack bound to the queue on the routing key
	Immediate   bool
	ContentType string
	// Transient or Persistent
	DeliveryMode uint8
	// Expiration time in ms that a message will expire from a queue.
	// See https://www.rabbitmq.com/ttl.html#per-message-ttl-in-publishers
	Expiration string
	Headers    Table
}

// WithPublishOptionsExchange returns a function that sets the exchange to publish to
func WithPublishOptionsExchange(exchange string) func(*PublishOptions) {
	return func(options *PublishOptions) {
		options.Exchange = exchange
	}
}

// WithPublishOptionsMandatory makes the publishing mandatory, which means when a queue is not
// bound to the routing key a message will be sent back on the returns channel for you to handle
func WithPublishOptionsMandatory(options *PublishOptions) {
	options.Mandatory = true
}

// WithPublishOptionsImmediate makes the publishing immediate, which means when a consumer is not available
// to immediately handle the new message, a message will be sent back on the returns channel for you to handle
func WithPublishOptionsImmediate(options *PublishOptions) {
	options.Immediate = true
}

// WithPublishOptionsContentType returns a function that sets the content type, i.e. "application/json"
func WithPublishOptionsContentType(contentType string) func(options *PublishOptions) {
	return func(options *PublishOptions) {
		options.ContentType = contentType
	}
}

// WithPublishOptionsPersistentDelivery sets the message to persist. Transient messages will
// not be stored to durable queues, persistent messages will be stored to durable queues and
// lost on non-durable queues during server request. By default publishing is transient
func WithPublishOptionsPersistentDelivery(options *PublishOptions) {
	options.DeliveryMode = Persistent
}

// WithPublishOptionsExpiration returns a function that sets the expiry/TTL of a message.
// As per RabbitMQ spec, string value in milliseconds.
func WithPublishOptionsExpiration(expiration string) func(options *PublishOptions) {
	return func(options *PublishOptions) {
		options.Expiration = expiration
	}
}

// WithPublishOptionsHeaders returns a function that sets message header values, i.e. "msg-id"
func WithPublishOptionsHeaders(headers Table) func(options *PublishOptions) {
	return func(options *PublishOptions) {
		options.Headers = headers
	}
}

// Publisher allows you to publish messages safely across an open connection
type Publisher struct {
	chManager                  *channelManager
	notifyReturnChan           chan Return
	disablePublishDueToFlow    bool
	disablePublishDueToFlowMux *sync.RWMutex
	logger                     *logrus.Entry
}

// PublisherOptions is used to describe a publisher's configuration.
// Logging set to true will enable the consumer to print to stdout
type PublisherOptions struct {
	Logging bool
	Logger  *logrus.Entry
}

// WithPublisherOptionsLogging sets logging to true on the consumer options
func WithPublisherOptionsLogging(options *PublisherOptions) {
	options.Logging = true
	options.Logger = logrus.WithContext(context.Background())
}

// WithPublisherOptionsLogger set logging to a custom interface.
// Use WithPublisherOptionsLogging to just log to stdout.
func WithPublisherOptionsLogger(logger *logrus.Entry) func(options *PublisherOptions) {
	return func(options *PublisherOptions) {
		options.Logging = true
		options.Logger = logger
	}
}

// NewPublisher returns a new publisher with an open channel to the cluster.
// If you plan to enforce mandatory or immediate publishing, those failures will be reported
// on the channel of Returns that you should setup a listener on.
// Flow controls are automatically handled as they are sent from the server, and publishing
// will fail with an error when the server is requesting a slowdown
func NewPublisher(url string, config amqp.Config, optionFunc ...func(options *PublisherOptions)) (
	*Publisher, <-chan Return, error,
) {
	options := &PublisherOptions{}
	for _, optionFunc := range optionFunc {
		optionFunc(options)
	}
	if options.Logger == nil {
		log := logrus.New()
		log.SetLevel(logrus.ErrorLevel)
		options.Logger = log.WithContext(context.Background())
	}

	chManager, err := newChannelManager(url, config, options.Logger)
	if err != nil {
		return nil, nil, err
	}

	publisher := &Publisher{
		chManager:                  chManager,
		notifyReturnChan:           make(chan Return, 1),
		disablePublishDueToFlow:    false,
		disablePublishDueToFlowMux: &sync.RWMutex{},
		logger:                     options.Logger,
	}

	go func() {
		publisher.startNotifyHandlers()
		for err := range publisher.chManager.notifyCancelOrClose {
			publisher.logger.Infof("publish cancel/close handler triggered. err: %v", err)
			publisher.startNotifyHandlers()
		}
	}()

	return publisher, publisher.notifyReturnChan, nil
}

func (p *Publisher) Publish(
	data []byte,
	routingKeys []string,
	optionFuncs ...func(options *PublishOptions),
) error {
	p.disablePublishDueToFlowMux.RLock()
	disablePublish := p.disablePublishDueToFlow
	p.disablePublishDueToFlowMux.RUnlock()

	if disablePublish {
		return errors.New("publishing blocked due to high flow on the server")
	}

	options := &PublishOptions{}
	for _, optionFunc := range optionFuncs {
		optionFunc(options)
	}

	if options.DeliveryMode == 0 {
		options.DeliveryMode = Transient
	}

	headers := tableToAMQPTable(options.Headers)
	for _, routingKey := range routingKeys {
		message := amqp.Publishing{}
		message.ContentType = options.ContentType
		message.DeliveryMode = options.DeliveryMode
		message.Body = data
		message.Headers = headers
		message.Expiration = options.Expiration

		// actual publish
		err := p.chManager.channel.Publish(
			options.Exchange,
			routingKey,
			options.Mandatory,
			options.Immediate,
			message,
		)
		if err != nil {
			return err
		}
	}

	return nil
}

// StopPublishing stops the publishing of messages.
// the publisher should be discarded as it's not safe for re-use
func (p *Publisher) StopPublishing() {
	p.chManager.Close()
}

func (p *Publisher) startNotifyHandlers() {
	returnAMQPChan := p.chManager.channel.NotifyReturn(make(chan amqp.Return, 1))
	go func() {
		for ret := range returnAMQPChan {
			p.notifyReturnChan <- Return{ret}
		}
	}()

	notifyFlowChan := p.chManager.channel.NotifyFlow(make(chan bool))
	go p.startNotifyFlowHandler(notifyFlowChan)
}

func (p *Publisher) startNotifyFlowHandler(notifyFlowChan chan bool) {
	p.disablePublishDueToFlowMux.Lock()
	p.disablePublishDueToFlow = false
	p.disablePublishDueToFlowMux.Unlock()

	// listeners for active=true flow control. When true is sent to a listener,
	// publishing should pause until false is sent to listeners.
	for ok := range notifyFlowChan {
		p.disablePublishDueToFlowMux.Lock()
		if ok {
			p.logger.Info("pausing publishing due to flow request from server")
			p.disablePublishDueToFlow = true
		} else {
			p.disablePublishDueToFlow = false
			p.logger.Info("resuming publishing due to flow request from server")
		}
		p.disablePublishDueToFlowMux.Unlock()
	}
}
