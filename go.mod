module consumer

go 1.16

require (
	github.com/caarlos0/env/v6 v6.6.2
	github.com/gin-gonic/gin v1.7.2
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/sirupsen/logrus v1.8.1
	github.com/streadway/amqp v1.0.0
	gitlab.com/goxp/cloud0 v1.4.5
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
