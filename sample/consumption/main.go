package main

import (
	"consumer/pkg/rabbitmq"
	"context"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

func main() {
	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)

	consumer, err := rabbitmq.NewConsumer(
		"amqp://guest:guest@localhost:5672/",
		amqp.Config{},
		rabbitmq.WithConsumerOptionsLogger(logger.WithContext(context.Background())),
	)
	if err != nil {
		logger.WithError(err).Error("failed to init consumer")
		os.Exit(1)
	}

	err = consumer.StartConsuming(
		func(d rabbitmq.Delivery) bool {
			logger.
				WithField("body", string(d.Body)).
				WithField("route", d.RoutingKey).
				Info("consumed")
			return true
		},
		"my_queue",
		[]string{"routing_key", "routing_key_2"},
		rabbitmq.WithConsumeOptionsConcurrency(10),
		rabbitmq.WithConsumeOptionsBindingExchangeDurable,
		rabbitmq.WithConsumeOptionsBindingExchangeName("events"),
		rabbitmq.WithConsumeOptionsBindingExchangeKind("topic"),
		rabbitmq.WithConsumeOptionsBindingExchangeDurable,
		rabbitmq.WithConsumeOptionsConsumerName("consumer1"),
	)
	if err != nil {
		logger.WithError(err).Error("failed to start consuming")
		os.Exit(1)
	}

	forever := make(chan struct{})
	<-forever
}
