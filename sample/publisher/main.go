package main

import (
	"consumer/pkg/rabbitmq"
	"context"
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

func createDefaultPublish(publisher *rabbitmq.Publisher) func(data []byte, routes []string) error {
	return func(data []byte, routes []string) error {
		return publisher.Publish(
			data,
			routes,
			rabbitmq.WithPublishOptionsContentType("application/json"),
			rabbitmq.WithPublishOptionsMandatory,
			rabbitmq.WithPublishOptionsPersistentDelivery,
			rabbitmq.WithPublishOptionsExchange("events"),
		)
	}
}

func exec(total int) int {
	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)

	publisher, _, err := rabbitmq.NewPublisher(
		"amqp://guest:guest@localhost:5672/",
		amqp.Config{},
		rabbitmq.WithPublisherOptionsLogger(logger.WithContext(context.Background())),
	)
	if err != nil {
		logger.WithError(err).Error("failed to init publisher")
		return 1
	}

	ticker := time.NewTicker(time.Millisecond * 10)
	counter := 0

	publishFunc := createDefaultPublish(publisher)

	for range ticker.C {
		counter++
		data := []byte(fmt.Sprintf("publisher message %d", counter))
		route := "echo.testing"

		err = publishFunc(data, []string{route})

		if err != nil {
			logger.WithError(err).Error("failed to publish message")
			return 1
		}

		if counter >= total {
			break
		}
	}

	return 0
}

func main() {
	total := 0
	flag.IntVar(&total, "total", 1, "number of messages to send")
	flag.Parse()

	os.Exit(exec(total))
}
