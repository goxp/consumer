package main

import (
	"consumer/pkg/service"
	"os"

	"gitlab.com/goxp/cloud0/logger"
)

func main() {
	err := service.Run()
	if err != nil {
		logger.Tag("main").Errorf("failed to init app: %v", err)
		os.Exit(1)
	}
}
